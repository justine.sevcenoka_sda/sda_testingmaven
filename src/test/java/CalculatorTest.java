import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class CalculatorTest {

    @Parameterized.Parameter()
    public int param1;

    @Parameterized.Parameter(1)
    public int param2;

    @Parameterized.Parameter(2)
    public int expectedSum;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private Calculator calculator;

    @Parameterized.Parameters
    public static Collection<Integer[]> parameters() {
        return Arrays.asList(new Integer[][]{
                {1, 1, 2},
                {2, 4, 6},
                {1, 6, 7},
                {4, 1, 5},
                {1, 3, 4},
                {0, 3, 3},
                {-1, 3, 2},
                {0, 0, 0}
        });
    }

    @Before
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void whenSumIsCorrect() {
        assertThat(calculator.add(param1, param2)).isEqualTo(expectedSum);
    }

    @Test(expected = Error.class)
    public void whenArgumentIsTooBigThenError() {
        calculator.add(2147483647, 1);
    }

    @Test
    public void substract() {
        assertThat(calculator.substract(2, 5)).isEqualTo(-3);
        assertThat(calculator.substract(23, 3)).isEqualTo(20);
    }

    @Test
    public void multiply() {
        assertThat(calculator.multiply(1, 3)).isEqualTo(3);
    }

    @Test(expected = ArithmeticException.class)
    public void shouldThrowExceptionWhenDividingBy0() {
        int number = new Random().nextInt();
        calculator.divide(number, 0);
    }
}