import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class FahrenheitCelsiusConverterTests {

    @Parameterized.Parameter(0)
    public int fromFahrenheit;

    @Parameterized.Parameter(1)
    public double expectedCelsius;

    @Parameterized.Parameter(2)
    public int fromCelsius;

    @Parameterized.Parameter(3)
    public double expectedFahrenheit;
    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private FahrenheitCelsiusConverter converter;

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {1, -17.22, 1000, 1832},
                {10, -12.22, 100, 212.0},
                {100, 37.78, 10, 50.0},
                {1000, 537.78, 0, 32.0}
        });
    }

    @Before
    public void setUp() {
        converter = new FahrenheitCelsiusConverter();
    }

    @Test
    public void toCelsius() {
        double actualCelsius = converter.toCelsius(fromFahrenheit);

        assertThat(actualCelsius).isEqualTo(expectedCelsius);
    }

    @Test
    public void toFahrenheit() {
        double actualFahrenheit = converter.toFahrenheit(fromCelsius);

        assertThat(actualFahrenheit).isEqualTo(expectedFahrenheit);
    }

    @Test
    public void expectedExceptionWhenBelowAbsoluteZeroForToCelsius() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Input value is below absolute zero");

        converter.toCelsius(-500);
    }

    @Test
    public void expectedExceptionWhenBelowAbsoluteZeroForToFahrenheit() {
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage("Input value is below absolute zero");

        converter.toFahrenheit(-300);
    }


}
