class Person {
    private int age;

    Person(int age) {
        this.age = age;
    }

    boolean isTeenager() {
        return age > 10 && age < 20;
    }
}
