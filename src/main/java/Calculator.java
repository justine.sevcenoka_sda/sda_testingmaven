class Calculator {
    private int usageCount;
    int add(int a, int b) {
        long result = (long) a + b;
        if (result > Integer.MAX_VALUE) {
            throw new Error();
        } else {
            return a + b;
        }
    }

    int substract(int x, int y) {
        return x - y;
    }

    int multiply(int x, int y) {
        return x * y;
    }

    int divide(int x, int y) {
        return x / y;
    }

    int getUsageCount() {
        return usageCount;
    }
}
