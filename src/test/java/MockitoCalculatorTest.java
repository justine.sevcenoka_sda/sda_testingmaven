import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class MockitoCalculatorTest {

    private Calculator calculator;

    @Test
    public void test() {
        calculator = mock(Calculator.class);
        when(calculator.add(1, 1)).thenReturn(42);
        assertThat(calculator.add(1, 1)).isEqualTo(42);

        when(calculator.add(anyInt(), anyInt())).thenReturn(5);
        assertThat(calculator.add(3, 98)).isEqualTo(5);

        verify(calculator, times(1)).add(eq(1), anyInt());
    }

    @Test
    public void testSpy() {
        Calculator myRealCalculator = new Calculator();
        Calculator spyCalculator = spy(myRealCalculator);

        when(spyCalculator.add(eq(4), eq(6))).thenReturn(55);

        assertThat(spyCalculator.add(1, 1)).isEqualTo(2);
        assertThat(spyCalculator.add(4, 6)).isEqualTo(55);
    }


}
