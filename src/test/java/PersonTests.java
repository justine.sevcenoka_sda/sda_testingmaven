import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(Parameterized.class)
public class PersonTests {

    @Parameterized.Parameter(0)
    public int age;
    @Parameterized.Parameter(1)
    public boolean expected;
    private Person person;

    @Parameterized.Parameters
    public static Collection<Object[]> parameters() {
        return Arrays.asList(new Object[][]{
                {0, false},
                {9, false},
                {10, false},
                {11, true},
                {19, true},
                {20, false},
                {21, false}
        });
    }

    @Before
    public void setUp() {
        person = new Person(age);
    }

    @Test
    public void isTeenager() {
        //given
        //when
        boolean actual = person.isTeenager();
        //then
        assertThat(actual).isEqualTo(expected);
    }

    @After
    public void tearDown() {
        person = null;
    }
}
