public class FahrenheitCelsiusConverter {
    public final double ABSOLUTE_FAHRENHEIT_ZERO = -459.67;
    public final double ABSOLUTE_CELSIUS_ZERO = -273.15;

    public double toCelsius(int fahrenheit) {
        if (fahrenheit < ABSOLUTE_FAHRENHEIT_ZERO) {
            throw new IllegalArgumentException("Input value is below absolute zero");
        } else {
            return Math.round(((fahrenheit - 32) / 1.8) * 100.0) / 100.0;
        }
    }

    public double toFahrenheit(int celsius) {
        if (celsius < ABSOLUTE_CELSIUS_ZERO) {
            throw new IllegalArgumentException("Input value is below absolute zero");
        } else {
            return Math.round((celsius * 1.8 + 32) * 100.0) / 100.0;
        }
    }
}
